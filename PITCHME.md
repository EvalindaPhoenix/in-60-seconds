---
@snap[north-east span-25]
![projet](/assets/img/AJC.png)
@snapend

@snap[north-west span-25]
![projet](/assets/img/Sogeti-logo-2018.png)
@snapend

@snap[midpoint span-95]
## @color[black](**Publication d'un site statique en CI/CD sur AWS**)
@snapend

@snap[south span-95]
<font size="4">Raouia BEN SALAH</font>
<font size="4">Kevin BRESSON</font>
<font size="4">Lina DOTE</font>
<font size="4">Mohamed LASSISSI</font>
<font size="4">Amar LOUNI</font>
<font size="4">Emilie PASTEUR</font>
<font size="4">Pascal RAMOS</font>
<font size="4">Assane SENE</font>
<font size="4">Sarah TAVERNEL</font>
<font size="4">Sarah TEBACHI</font>
<font size="4">Stéphanie VALERE</font>
@snapend

---?image=/assets/img/trombinoscope.png

---?image=assets/img/2foot.png&size=auto&color=#CCDAE7

---
@snap[north span-90]
## Outils d organisation
@snapend

@snap[east span-60]
![Photo-outils-orga](/assets/img/logo-monday.png)
@snapend

@snap[west span-40]
![Photo-outils-orga](/assets/img/Slack-logo.png)
@snapend

---?image=assets/img/3listingtache.png&size=auto&color=#CCDAE7


---
# partie infrastructure





---
# partie developpement
---?image=assets/img/1intro.png&size=auto&color=#CCDAE7 
---?image=assets/img/4visiondev-ops.png&size=auto&color=#CCDAE7 
---?image=assets/img/5gitlab.png&size=auto&color=#CCDAE7 
---?image=assets/img/6.png&size=auto&color=#CCDAE7 
---?image=assets/img/7.png&size=auto&color=#CCDAE7 
---?image=assets/img/8.png&size=auto&color=#CCDAE7 
---?image=assets/img/9.png&size=auto&color=#CCDAE7 
---?image=assets/img/91.png&size=auto&color=#CCDAE7


